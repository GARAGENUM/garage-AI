# GARAGE GPT :alien:

![CHATGPT](https://media2.giphy.com/media/qAtZM2gvjWhPjmclZE/giphy.gif?cid=ecf05e47pji6o2vjk5sa2thp1f8yqjtywlt7vm9m45nqykzx&ep=v1_gifs_search&rid=giphy.gif&ct=g)

Projet pour héberger un Chat-GPT local s'appuyant sur les projets [Ollama](https://github.com/ollama/ollama) et [OpenWebUI](https://github.com/open-webui/open-webui)

## PREREQUIS :paperclip:

- Docker :whale:

## MODELS :moyai:

- compatibles: [Ollama modèles librairie](https://ollama.com/library)  

## CONFIGURATION :wrench:

### BASE

Nous pouvons laisser OpenWebUI gérer les comptes utilisateur, le premier utilisateur étant d'office administrateur.

- Modifier la variable à "true" si on utilise pas OICD:
```yml
      - "ENABLE_LOGIN_FORM=false"
```

### OIDC :key:

Renseigner les variables d'environnement dans le docker-compose.yml:
```yml
      - "ENABLE_OPENAI_API=true"
      - "OPENAI_API_KEY="
      - "OAUTH_CLIENT_ID=ai"
      - "OAUTH_CLIENT_SECRET=redacted"
      - "OPENID_PROVIDER_URL=https://keycloak/auth/realms/realm/.well-known/openid-configuration"
      - "OAUTH_PROVIDER_NAME=keycloak"
      - "OAUTH_SCOPES=openid profile email"
```

### GPU USE

- Installer NVIDIA container toolkit:
```bash
curl -fsSL https://nvidia.github.io/libnvidia-container/gpgkey | sudo gpg --dearmor -o /usr/share/keyrings/nvidia-container-toolkit-keyring.gpg \
  && curl -s -L https://nvidia.github.io/libnvidia-container/stable/deb/nvidia-container-toolkit.list | \
    sed 's#deb https://#deb [signed-by=/usr/share/keyrings/nvidia-container-toolkit-keyring.gpg] https://#g' | \
    sudo tee /etc/apt/sources.list.d/nvidia-container-toolkit.list

sed -i -e '/experimental/ s/^#//g' /etc/apt/sources.list.d/nvidia-container-toolkit.list
sudo apt-get update
sudo apt-get install -y nvidia-container-toolkit
```

- Décommenter la section suivante:
```yml
    deploy:
      resources:
        reservations:
          devices:
            - driver: nvidia
              count: 1
              capabilities: [gpu]
```

## UTILISATION :checkered_flag:

- Démarrer la stack:
```bash
docker compose up -d
```

- Ajouter des modèles:

![ill-1](docs/modèles.png)

- Choisir les modèles disponible pour les utilisateurs:

![ill-2](docs/workspace.png)

## DOCUMENTATION :books:

- [Ollama](https://github.com/ollama/ollama/tree/main/docs)  

- [OpenWebUI](https://docs.openwebui.com/)

- [NVIDIA container toolkit](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/latest/install-guide.html)